// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class AnsiEscapeCodeTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testBrightRedForegroundBold() {
		final AnsiEscapeCode[] theCodes = { AnsiEscapeCode.FG_BRIGHT_RED, AnsiEscapeCode.BOLD };
		String theSequence = AnsiEscapeCode.toEscapeSequence( theCodes );
		theSequence = AnsiEscapeCode.toEscapedSequence( theSequence );
		if ( IS_LOG_TESTS_ENABLED ) {
			AnsiEscapeCode eCode;
			for ( int i = 0; i < theCodes.length; i++ ) {
				eCode = theCodes[i];
				System.out.print( eCode );
				if ( i < theCodes.length - 1 ) {
					System.out.print( "+" );
				}
			}
			System.out.println( "=" + theSequence );
		}
		assertEquals( "u001B[91;1m", theSequence );
	}

	@Test
	public void testBrightWhiteForegroundBrightRedBackground() {
		final AnsiEscapeCode[] theCodes = { AnsiEscapeCode.FG_BRIGHT_WHITE, AnsiEscapeCode.BG_BRIGHT_RED };
		String theSequence = AnsiEscapeCode.toEscapeSequence( theCodes );
		theSequence = AnsiEscapeCode.toEscapedSequence( theSequence );
		if ( IS_LOG_TESTS_ENABLED ) {
			AnsiEscapeCode eCode;
			for ( int i = 0; i < theCodes.length; i++ ) {
				eCode = theCodes[i];
				System.out.print( eCode );
				if ( i < theCodes.length - 1 ) {
					System.out.print( "+" );
				}
			}
			System.out.println( "=" + theSequence );
		}
		assertEquals( "u001B[97;101m", theSequence );
	}

	@Test
	public void testReset() {
		final AnsiEscapeCode[] theCodes = { AnsiEscapeCode.RESET };
		String theSequence = AnsiEscapeCode.toEscapeSequence( theCodes );
		theSequence = AnsiEscapeCode.toEscapedSequence( theSequence );
		if ( IS_LOG_TESTS_ENABLED ) {
			AnsiEscapeCode eCode;
			for ( int i = 0; i < theCodes.length; i++ ) {
				eCode = theCodes[i];
				System.out.print( eCode );
				if ( i < theCodes.length - 1 ) {
					System.out.print( "+" );
				}
			}
			System.out.println( "=" + theSequence );
		}
		assertEquals( "u001B[0m", theSequence );
	}
}
