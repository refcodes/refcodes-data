// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.junit.jupiter.api.Test;

public class FilenameExtensionTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String[] FILES = { ".hallo", "hallo", "hallo.txt", "hallo.tar.gz", "hallo.png", "hallo.chaos64", "/hallo", "./hallo.txt", "~/temp/hallo.tar.gz", "/mnt/transfer/hallo.png", "C:\\Users\\5161\\hallo.chaos64", ".hallo.", "./hallo/du/da/hallo." };

	@Test
	public void testRawFileNameExtension() {
		String eExtension;
		for ( String eFilename : FILES ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				eExtension = FilenameExtension.toRawFileNameExtension( eFilename );
				System.out.println( eExtension + " <-- " + eFilename );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testFileNameExtension() {
		FilenameExtension eExtension;
		for ( String eFilename : FILES ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				eExtension = FilenameExtension.toFileNameExtension( eFilename );
				System.out.println( eExtension + " <-- " + eFilename );
			}
		}
	}
}
