// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import static org.junit.jupiter.api.Assertions.*;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class SchemeTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String[][] RESULTS = new String[][] {
		// @formatter:off
		{ "jar", "jar:", ".jar!" },
		{ "zip", "zip:", ".zip!" }, 
		{ "sh", "sh:", ".sh!" },
		{ "file", "file:", "file:/" },
		{ "http", "http://", "http://" },
		{ "https", "https://", "https://" },
		{ "socks", "socks://", "socks://" },
		{ "socks4", "socks4://", "socks4://" },
		{ "socks5", "socks5://", "socks5://" },
		{ "???", "???:", "???:" }
		// @formatter:on
	};

	private static Map<Scheme, String[]> SCHEMES;

	// /////////////////////////////////////////////////////////////////////////
	// SETUP:
	// /////////////////////////////////////////////////////////////////////////

	@BeforeAll
	public static void beforeAll() {
		SCHEMES = new HashMap<>();
		for ( String[] aRESULTS : RESULTS ) {
			final Scheme eScheme = Scheme.fromName( aRESULTS[0] );
			SCHEMES.put( eScheme, aRESULTS );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testScheme() {
		for ( Scheme eScheme : Scheme.values() ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eScheme.getName() + ", " + eScheme.toProtocol() + ", " + eScheme.getMarker() );
			}
			final String[] eExpected = SCHEMES.get( eScheme );
			assertEquals( eExpected[0], eScheme.getName() );
			assertEquals( eExpected[1], eScheme.toProtocol() );
			assertEquals( eExpected[2], eScheme.getMarker() );
		}
	}
}
