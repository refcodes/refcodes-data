module org.refcodes.data {
	requires org.refcodes.licensing;
	requires transitive org.refcodes.mixin;

	exports org.refcodes.data;
}
