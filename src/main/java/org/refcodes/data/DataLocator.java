// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import java.io.InputStream;
import java.net.URL;

/**
 * Provides an accessor for a data locator property ({@link URL} and
 * {@link InputStream}).
 */
public interface DataLocator {

	/**
	 * Gets the data url.
	 *
	 * @return the data url
	 */
	URL getDataUrl();

	/**
	 * Gets the data input stream.
	 *
	 * @return the data input stream
	 */
	InputStream getDataInputStream();

	/**
	 * Provides a mutator for a data locator property.
	 */
	public interface DataLocatorMutator {

		/**
		 * Sets the data url.
		 *
		 * @param aUrl the new data url
		 */
		void setDataUrl( URL aUrl );

		/**
		 * Sets the data input stream.
		 *
		 * @param aInputStream the new data input stream
		 */
		void setDataInputStream( InputStream aInputStream );
	}

	/**
	 * Provides a builder for a data locator property.
	 *
	 * @param <B> the generic type
	 */
	public interface DataLocatorBuilder<B extends DataLocatorBuilder<B>> {

		/**
		 * With data url.
		 *
		 * @param aUrl the URL
		 * 
		 * @return the b
		 */
		B withDataUrl( URL aUrl );

		/**
		 * With data input stream.
		 *
		 * @param aInputStream the input stream
		 * 
		 * @return the b
		 */
		B withDataInputStream( InputStream aInputStream );
	}

	/**
	 * Provides a property (getter / setter) for a data locator property (width
	 * and height).
	 */
	public interface DataLocatorProperty extends DataLocator, DataLocatorMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link URL} array
		 * (setter) as of {@link #setDataUrl(URL)} and returns the very same
		 * value (getter).
		 * 
		 * @param aDataUrl The {@link URL} array to set (via
		 *        {@link #setDataUrl(URL)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default URL letDataUrl( URL aDataUrl ) {
			setDataUrl( aDataUrl );
			return aDataUrl;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link InputStream}
		 * array (setter) as of {@link #setDataInputStream(InputStream)} and
		 * returns the very same value (getter).
		 * 
		 * @param aDataInputStream The {@link InputStream} array to set (via
		 *        {@link #setDataInputStream(InputStream)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default InputStream letDataInputStream( InputStream aDataInputStream ) {
			setDataInputStream( aDataInputStream );
			return aDataInputStream;
		}
	}
}
