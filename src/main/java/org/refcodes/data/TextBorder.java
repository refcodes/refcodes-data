// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.refcodes.mixin.CharAccessor;

/**
 * The Enum TextBorder. Also see
 * <code>http://www.roysac.com/tutorial/roy-blockasciitutorial.html</code>
 */
public enum TextBorder implements CharAccessor {

	// @formatter:off
	SINGLE_TOP_LEFT_EDGE ( '┌'),
	
	SINGLE_TOP_DIVIDER_EDGE ( '┬'),
	
	SINGLE_TOP_RIGHT_EDGE ( '┐'),
	
	SINGLE_VERTICAL_LINE ( '│'),
	
	SINGLE_HORIZONTAL_LINE ( '─'),
	
	SINGLE_BOTTOM_LEFT_EDGE ( '└'),
	
	SINGLE_BOTTOM_DIVIDER_EDGE ( '┴'),
	
	SINGLE_BOTTOM_RIGHT_EDGE ( '┘'),
	
	SINGLE_LEFT_EDGE ( '├'),
	
	SINGLE_RIGHT_EDGE ( '┤'),
	
	SINGLE_DIVIDER_EDGE ( '┼'),

	DOUBLE_TOP_LEFT_EDGE ( '╔'),
	
	DOUBLE_TOP_DIVIDER_EDGE ( '╦'),
	
	DOUBLE_TOP_RIGHT_EDGE ( '╗'),
	
	DOUBLE_VERTICAL_LINE ( '║'),
	
	DOUBLE_HORIZONTAL_LINE ( '═'),
	
	DOUBLE_BOTTOM_LEFT_EDGE ( '╚'),
	
	DOUBLE_BOTTOM_DIVIDER_EDGE ( '╩'),
	
	DOUBLE_BOTTOM_RIGHT_EDGE ( '╝'),
	
	DOUBLE_LEFT_EDGE ( '╠'),
	
	DOUBLE_RIGHT_EDGE ( '╣'),
	
	DOUBLE_DIVIDER_EDGE ( '╬'),

	SINGLE_DOUBLE_TOP_LEFT_EDGE ( '╓'),
	
	SINGLE_DOUBLE_TOP_DIVIDER_EDGE ( '╥'),
	
	SINGLE_DOUBLE_TOP_RIGHT_EDGE ( '╖'),
	
	SINGLE_DOUBLE_VERTICAL_LINE ( '║'),
	
	SINGLE_DOUBLE_HORIZONTAL_LINE ( '─'),
	
	SINGLE_DOUBLE_BOTTOM_LEFT_EDGE ( '╙'),
	
	SINGLE_DOUBLE_BOTTOM_DIVIDER_EDGE ( '╨'),
	
	SINGLE_DOUBLE_BOTTOM_RIGHT_EDGE ( '╜'),
	
	SINGLE_DOUBLE_LEFT_EDGE ( '╟'),
	
	SINGLE_DOUBLE_RIGHT_EDGE ( '╢'),
	
	SINGLE_DOUBLE_DIVIDER_EDGE ( '╫'),

	DOUBLE_SINGLE_TOP_LEFT_EDGE ( '╒'),
	
	DOUBLE_SINGLE_TOP_DIVIDER_EDGE ( '╤'),
	
	DOUBLE_SINGLE_TOP_RIGHT_EDGE ( '╕'),
	
	DOUBLE_SINGLE_VERTICAL_LINE ( '│'),
	
	DOUBLE_SINGLE_HORIZONTAL_LINE ( '═'),
	
	DOUBLE_SINGLE_BOTTOM_LEFT_EDGE ( '╘'),
	
	DOUBLE_SINGLE_BOTTOM_DIVIDER_EDGE ( '╧'),
	
	DOUBLE_SINGLE_BOTTOM_RIGHT_EDGE ( '╛'),
	
	DOUBLE_SINGLE_LEFT_EDGE ( '╞'),
	
	DOUBLE_SINGLE_RIGHT_EDGE ( '╡'),
	
	DOUBLE_SINGLE_DIVIDER_EDGE ( '╪'),

	ASCII_TOP_LEFT_EDGE ( '/'),
	
	ASCII_TOP_DIVIDER_EDGE ( '+'),
	
	ASCII_TOP_RIGHT_EDGE ( '\\'),
	
	ASCII_VERTICAL_LINE ( '|'),
	
	ASCII_HORIZONTAL_LINE ( '-'),
	
	ASCII_BOTTOM_LEFT_EDGE ( '\\'),
	
	ASCII_BOTTOM_DIVIDER_EDGE ( '+'),
	
	ASCII_BOTTOM_RIGHT_EDGE ( '/'),
	
	ASCII_LEFT_EDGE ( '+'),
	
	ASCII_RIGHT_EDGE ( '+'),
	
	ASCII_DIVIDER_EDGE ( '+');
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private char _textBorderChar;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new text border.
	 *
	 * @param aTextBorderChar the text border char
	 */
	private TextBorder( char aTextBorderChar ) {
		_textBorderChar = aTextBorderChar;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getChar() {
		return _textBorderChar;
	}
}