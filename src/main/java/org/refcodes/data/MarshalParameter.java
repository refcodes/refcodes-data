// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.refcodes.mixin.NameAccessor;

/**
 * {@link MarshalParameter} defines common parameters (parameter is considered
 * the key of a key/value pair) of a given semantics for the according property.
 */
public enum MarshalParameter implements NameAccessor {

	/**
	 * Identifies a charset code such as {@link Encoding#UTF_8}'s
	 * {@link Encoding#getCode()}.
	 */
	CHARSET("charset"),

	/**
	 * A base URL may be stored in such a property.
	 */
	BASE_URL("baseUrl"),

	/**
	 * The value of such a property identifies a single delimiter char.
	 */
	DELIMITER("delimiter"),

	/**
	 * The value of such a property identifies a multiple delimiter chars.
	 */
	DELIMITERS("delimiters"),

	/**
	 * The value of such a property determines whether unmarshaling (XML)
	 * documents will preserve the root element (envelope) even when merely
	 * acting as an envelope. As an (XML) document requires a root element, the
	 * root element often is provided merely as of syntactic reasons and must be
	 * omitted as of semantic reasons. Unmarshaling functionality therefore by
	 * default skips the root elelemt, as this is considered merely to serve as
	 * an envelope. This behavior can be overridden by setting this property to
	 * <code>true</code>.
	 */
	DOCUMENT_ENVELOPE("documentEnvelope"),

	/**
	 * The value of such a property determines whether marshaling (XML)
	 * documents will preserve array index information by using an index
	 * attribute for array elements in the produced XML.
	 */
	DOCUMENT_ARRAY_INDEX("documentArrayIndex"),

	/**
	 * Identifies a comment {@link String}.
	 */
	COMMENT("comment"),

	/**
	 * Identifies the encoding such as {@link Encoding#UTF_8}.
	 */
	ENCODING("encoding"),

	/**
	 * Identifies a version, such as an XML version (depends on the notation to
	 * which to marshal).
	 */
	VERSION("version"),

	/**
	 * The root element to be used when marshaling a structure to a notation
	 * which requires a root element.
	 */
	ROOT_ELEMENT("rootElement"),

	/**
	 * E.g. used in an XML file's head's declaration such as
	 * <code>&lt;?xml ecnoding="UTF-8" version="1.0" standalone="yes"?&gt;</code>
	 * to determine whether this is a stand alone document (note that it is
	 * declared as "standalone"!).
	 */
	STAND_ALONE("standalone"),

	/**
	 * E.g. used in an XML file's head's declaration such as
	 * <code>&lt;?xml ecnoding="UTF-8" version="1.0" standalone="yes"?&gt;</code>
	 */
	HEADER("header");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _parameterName;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new property.
	 *
	 * @param apropertyName the property name
	 */
	private MarshalParameter( String apropertyName ) {
		_parameterName = apropertyName;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return _parameterName;
	}
}