// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import java.io.InputStream;
import java.net.URL;

/**
 * Provides an accessor for a data locators property (array of {@link URL}s and
 * {@link InputStream}s).
 */
public interface DataArrayLocator {

	/**
	 * Gets the data urls.
	 *
	 * @return the data urls
	 */
	URL[] getDataUrls();

	/**
	 * Gets the data input streams.
	 *
	 * @return the data input streams
	 */
	InputStream[] getDataInputStreams();

	/**
	 * Provides a mutator for a data locators property.
	 */
	public interface DataLocatorsMutator {

		/**
		 * Sets the data urls.
		 *
		 * @param aUrls the new data urls
		 */
		void setDataUrls( URL[] aUrls );

		/**
		 * Sets the data input streams.
		 *
		 * @param aInputStream the new data input streams
		 */
		void setDataInputStreams( InputStream[] aInputStream );
	}

	/**
	 * Provides a builder for a data locators property.
	 *
	 * @param <B> the generic type
	 */
	public interface DataLocatorsBuilder<B extends DataLocatorsBuilder<B>> {

		/**
		 * With data urls.
		 *
		 * @param aUrls the urls
		 * 
		 * @return the b
		 */
		B withDataUrls( URL[] aUrls );

		/**
		 * With data input streams.
		 *
		 * @param aInputStreams the input streams
		 * 
		 * @return the b
		 */
		B withDataInputStreams( InputStream[] aInputStreams );
	}

	/**
	 * Provides a property (getter / setter) for a data locators property (width
	 * and height).
	 */
	public interface DataLocatorsProperty extends DataArrayLocator, DataLocatorsMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link URL} array
		 * (setter) as of {@link #setDataUrls(URL[])} and returns the very same
		 * value (getter).
		 * 
		 * @param aDataUrls The {@link URL} array to set (via
		 *        {@link #setDataUrls(URL[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default URL[] letDataUrls( URL[] aDataUrls ) {
			setDataUrls( aDataUrls );
			return aDataUrls;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link InputStream}
		 * array (setter) as of {@link #setDataInputStreams(InputStream[])} and
		 * returns the very same value (getter).
		 * 
		 * @param aDataInputStreams The {@link InputStream} array to set (via
		 *        {@link #setDataInputStreams(InputStream[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default InputStream[] letDataInputStreams( InputStream[] aDataInputStreams ) {
			setDataInputStreams( aDataInputStreams );
			return aDataInputStreams;
		}
	}
}
