package org.refcodes.data;

import org.refcodes.mixin.PortAccessor;

/**
 * The Enum Port.
 */
public enum Port implements PortAccessor {

	/** File Transfer Protocol (FTP):. */
	FTP(21),
	/** Secure Shell (SSH):. */
	SSH(22),
	/** Telnet remote login service:. */
	TELNET(23),
	/** Simple Mail Transfer Protocol (SMTP):. */
	SMTP(25),
	/** Domain Name System (DNS) service:. */
	DNS(53),
	/** Hypertext Transfer Protocol (HTTP) used in the World Wide Web:. */
	HTTP(80),
	/** Post Office Protocol (POP3):. */
	POP3(110),
	/** Network News Transfer Protocol (NNTP):. */
	NNTP(119),
	/** Network Time Protocol (NTP):. */
	NTP(123),
	/** Internet Message Access Protocol (IMAP):. */
	IMAP(143),
	/** Simple Network Management Protocol (SNMP):. */
	SNMP(161),
	/** Internet Relay Chat (IRC):. */
	IRC(194),
	/** HTTP Secure (HTTPS):. */
	HTTPS(443),
	/**
	 * REFCODES.ORG:
	 */
	REFCODES(5161),
	/** HTTP alternate (http_alt). */
	HTTP_ALTERNATE(8080);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _port;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new port.
	 *
	 * @param aPort the port value
	 */
	private Port( int aPort ) {
		_port = aPort;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */

	@Override
	public int getPort() {
		return _port;
	}
}
