// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

/**
 * An enumeration for text palette definitions to make it easy working with such
 * default palettes. Such palettes can be used when drawing ASCII art, each
 * character element in a palette represents a gray scale, from bright (first
 * element) to dark (last element).
 */
public enum AsciiColorPalette {

	/**
	 * This palate contains a minimal set of characters from white to black
	 * representing gray scale colors in terms of ASCII art.
	 */
	MIN_LEVEL_GRAY(new char[] { ' ', '.', '-', '*', '#' }),

	/**
	 * This palate contains a normal set of characters from white to black
	 * representing gray scale colors in terms of ASCII art.
	 */
	NORM_LEVEL_GRAY(new char[] { ' ', '.', ':', '-', '=', '+', '*', '#', '%', '@' }),

	/**
	 * This palate contains the full set of characters from white to black
	 * representing gray scale colors in terms of ASCII art.
	 */
	MAX_LEVEL_GRAY(new char[] { ' ', '.', '\'', '`', '^', '"', ',', ':', ';', 'I', 'l', '!', 'i', '>', '<', '~', '+', '_', '-', '?', ']', '[', '}', '{', '1', ')', '(', '|', '\\', '/', 't', 'f', 'j', 'r', 'x', 'n', 'u', 'v', 'c', 'z', 'X', 'Y', 'U', 'J', 'C', 'L', 'Q', '0', 'O', 'Z', 'm', 'w', 'q', 'p', 'd', 'b', 'k', 'h', 'a', 'o', '*', '#', 'M', 'W', '&', '8', '%', 'B', '@', '$' }),

	/**
	 * This palate contains block characters representing shade patterns from
	 * white to black representing gray scale colors in terms of ASCII art.
	 */
	HALFTONE_GRAY(new char[] { ' ', '░', '▒', '▓', '█' }),

	/**
	 * This palate contains block characters representing shade patterns from
	 * white to black representing gray scale colors in terms of ASCII art.
	 */
	MONOCHROME(new char[] { ' ', '█', });

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private char[] _palette;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the palette with the given characters.
	 * 
	 * @param aPalette The palette to be used for the according enumartation
	 *        element.
	 */
	private AsciiColorPalette( char[] aPalette ) {
		_palette = aPalette;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the palette for the according enumeration element.
	 * 
	 * @return The according palette.
	 */
	public char[] getPalette() {
		return _palette;
	}

	/**
	 * Returns the palette for the according enumeration element.
	 * 
	 * @return The according palette.
	 */
	public char[] toInverse() {
		final char[] theResult = new char[_palette.length];
		for ( int i = 0; i < _palette.length; i++ ) {
			theResult[i] = _palette[_palette.length - 1 - i];
		}
		return theResult;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the enumeration element representing the given palette name
	 * (ignoring the case) or null if none was found.
	 * 
	 * @param aPaletteName The palette name for which to get the enumeration
	 *        element.
	 * 
	 * @return The enumeration element determined or null if none matching was
	 *         found.
	 */
	public static AsciiColorPalette fromPaletteName( String aPaletteName ) {
		for ( AsciiColorPalette eValue : values() ) {
			if ( eValue.name().equalsIgnoreCase( aPaletteName ) ) {
				return eValue;
			}
		}
		return null;
	}
}
