// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

/**
 * Some values's literal ({@link String}) representation useful when parsing or
 * evaluating texts.
 */
public enum BooleanLiterals {

	/**
	 * Strings representing a "true" boolean value.
	 */
	TRUE(new String[] { Literal.TRUE.getValue(), Literal.YES.getValue(), Literal.ON.getValue(), "1" }),
	/**
	 * Strings representing a "false" boolean value.
	 */
	FALSE(new String[] { Literal.FALSE.getValue(), Literal.NO.getValue(), Literal.OFF.getValue(), "0" });

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String[] _literalNames;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new literals.
	 *
	 * @param aLiteralNames the literal names
	 */
	private BooleanLiterals( String[] aLiteralNames ) {
		_literalNames = aLiteralNames;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * An array of the names representing the set of folder names.
	 * 
	 * @return The array of folder names representing this set.
	 */
	public String[] getNames() {
		return _literalNames;
	}

	/**
	 * Returns true in case the given {@link String} equals (ignoring the case)
	 * one of the <code>true</code> literals as of {@link #TRUE} and
	 * {@link #getNames()}.
	 * 
	 * @param aValue The value to be tested whether it represents a
	 *        <code>true</code> literal.
	 * 
	 * @return True in case we have a <code>true</code> literal, else false.
	 */
	public static boolean isTrue( String aValue ) {
		for ( String eLiteral : TRUE.getNames() ) {
			if ( eLiteral.equalsIgnoreCase( aValue ) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns true in case the given {@link String} equals (ignoring the case)
	 * one of the <code>false</code> literals as of {@link #FALSE} and
	 * {@link #getNames()}.
	 * 
	 * @param aValue The value to be tested whether it represents a
	 *        <code>false</code> literal.
	 * 
	 * @return True in case we have a <code>false</code> literal, else false.
	 */
	public static boolean isFalse( String aValue ) {
		for ( String eLiteral : FALSE.getNames() ) {
			if ( eLiteral.equalsIgnoreCase( aValue ) ) {
				return true;
			}
		}
		return false;
	}
}
