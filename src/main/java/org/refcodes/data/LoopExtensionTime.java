// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied), as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.refcodes.mixin.TimeMillisAccessor;

/**
 * The {@link LoopExtensionTime} defined values used inside a control flow.
 */
public enum LoopExtensionTime implements TimeMillisAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CODE LOOP SLEEP TIMES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * A retry is being extended for each retry by an exponential delay
	 * extension time (when &gt; 0): long theDelay((long) ( Math.pow(4,
	 * theRetries++ ) * EXTENSION_TIME ))), Also see
	 * <code>http://awsdocs.s3.amazonaws.com/SDB/latest/sdb-dg.pdf</code>
	 */
	MIN(50),

	/**
	 * A retry is being extended for each retry by an exponential delay
	 * extension time (when &gt; 0): long theDelay((long) ( Math.pow(4,
	 * theRetries++ ) * EXTENSION_TIME ))), Also see
	 * <code>http://awsdocs.s3.amazonaws.com/SDB/latest/sdb-dg.pdf</code>
	 */
	NORM(100),

	/**
	 * A retry is being extended for each retry by an exponential delay
	 * extension time (when &gt; 0): long theDelay((long) ( Math.pow(4,
	 * theRetries++ ) * EXTENSION_TIME ))), See also
	 * <code>http://awsdocs.s3.amazonaws.com/SDB/latest/sdb-dg.pdf</code>
	 */
	MAX(130);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _value;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new loop extension time.
	 *
	 * @param aValue the value
	 */
	private LoopExtensionTime( int aValue ) {
		_value = aValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getTimeMillis() {
		return _value;
	}
}
