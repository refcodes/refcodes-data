// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.refcodes.mixin.TextAccessor;

/**
 * Some licensing and copyright information as {@link String} constants.
 */
public enum License implements TextAccessor {

	// @formatter:off
	REFCODES_LICENSE(  
		  "=============================================================================\n"
		+ "REFCODES.ORG\n"
		+ "=============================================================================\n"
		+ "This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed\n"
		+ "under the following (see \"http://en.wikipedia.org/wiki/Multi-licensing\")\n"
		+ "licenses:\n"
		+ "=============================================================================\n"
		+ "GNU General Public License, v3.0 (\"http://www.gnu.org/licenses/gpl-3.0.html\")\n"
		+ "=============================================================================\n"
		+ "Apache License, v2.0 (\"http://www.apache.org/licenses/TEXT-2.0\")\n"
		+ "=============================================================================\n"
		+ "Please contact the copyright holding author(s) of the software artifacts in\n"
		+ "question for licensing issues not being covered by the above listed licenses,\n"
		+ "also regarding commercial licensing models or regarding the compatibility\n"
		+ "with other open source licenses.\n"
		+ "=============================================================================\n"
	),
	
	// @formatter:on
	LICENSE_NOTE("Licensed under GNU General Public License, v3.0 and Apache License, v2.0"),

	COPYRIGHT_NOTE("Copyright (c) by REFCODES.ORG, Munich, Germany.");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _text;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new license.
	 *
	 * @param aText the text
	 */
	private License( String aText ) {
		_text = aText;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getText() {
		return _text;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return _text;
	}
}
