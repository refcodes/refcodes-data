// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.refcodes.mixin.CharAccessor;

/**
 * Commonly used characters needful for analyzing, parsing or truncating texts.
 */
public enum QuotationMark implements CharAccessor {

	// @formatter:off
	SINGLE_QUOTE('\''),
	
	DOUBLE_QUOTE('"'),
	
	SINGLE_GUILLEMET_BEGIN('‹'),
	
	SINGLE_GUILLEMET_END('›'),
	
	DOUBLE_GUILLEMET_BEGIN('«'),
	
	DOUBLE_GUILLEMET_END('»'),
	
	SINGLE_CITATION_BEGIN('‘'),
	
	SINGLE_CITATION_END('’'),
	
	DOUBLE_CITATION_BEGIN('“'),
	
	DOUBLE_CITATION_END('”');
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private char _char;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new quotation mark.
	 *
	 * @param aChar the char
	 */
	private QuotationMark( char aChar ) {
		_char = aChar;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getChar() {
		return _char;
	}
}
