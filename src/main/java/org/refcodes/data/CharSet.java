// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.refcodes.mixin.CharSetAccessor;

/**
 * Commonly used char sets.
 */
public enum CharSet implements CharSetAccessor {

	ALPHABETIC(new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' }),

	ALPHANUMERIC(new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' }),

	ARABIC_BASE64(new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '+', '/' }),

	ASCII(new char[] { ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~' }),

	ASCII_SPECIAL(new char[] { ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^', '_', '`', '{', '|', '}', '~' }),

	BASE64(new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/' }),

	BASE64URL(new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_' }),

	BINARY(new char[] { '0', '1' }),

	/**
	 * Characters representing closing braces.
	 */
	CLOSING_BRACES(new char[] { ')', ']', '}' }),

	DECIMAL(new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' }),

	/**
	 * No prefixing zeros allowed when encoding a value as a number (e.g. int).
	 */
	ENCODED_AS_NUMBER(new char[] { '1', '2', '3', '4', '5', '6', '7', '8', '9' }),

	END_OF_LINE(new char[] { '\n', '\f' }),

	/**
	 * Declares the characters denoting an end of a sentence.
	 */
	END_OF_SENTENCE(new char[] { '.', '?', '!' }),

	ESCAPE_SEQUENCES(new char[] { '\t', '\b', '\n', '\r', '\f', '\'', '\"', '\\' }),

	HEXADECIMAL(new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' }),

	/**
	 * When formatting texts, those chars can be used to identify a possible
	 * suitable end of line (when seeking for a position to do a line-break).
	 */
	LINE_BREAK_MARKERS(new char[] { '=', '#', '.', ',', '?', '!', ';', ':', '\t', '\f' }),

	LOWER_CASE(new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' }),

	NUMERIC(new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' }),

	OCTAL(new char[] { '0', '1', '2', '3', '4', '5', '6', '7' }),

	/**
	 * Characters representing opening braces.
	 */
	OPENING_BRACES(new char[] { '(', '[', '{' }),

	QUOTES(new char[] { QuotationMark.SINGLE_QUOTE.getChar(), '`', '"' }),

	REGEX_SPECIAL_CHARS(new char[] { '.', '^', '$', '*', '+', '?', '(', ')', '[', '{', '\\', '|' }),

	/**
	 * When formatting texts, those chars can be used to identify a space
	 * between two words.
	 */
	SPACE_MARKERS(new char[] { ' ', '\t', '\n', '\f' }),

	UPPER_CASE(new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' }),

	WHITE_SPACES(new char[] { ' ', '\t', '\b', '\n', '\f', '\r' });

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private char[] _charSet;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new char set.
	 *
	 * @param aCharSet the char set
	 */
	private CharSet( char[] aCharSet ) {
		_charSet = aCharSet;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char[] getCharSet() {
		return _charSet;
	}

	/**
	 * Checks for char.
	 *
	 * @param aChar the char
	 * 
	 * @return true, if successful
	 */
	public boolean hasChar( char aChar ) {
		for ( char eChar : _charSet ) {
			if ( eChar == aChar ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the charset which's name matches the provided name, ignoring the
	 * case and the underscore ("_").
	 * 
	 * @param aCharsetName The name of the charset.
	 * 
	 * @return The according {@link CharSet} or null if none matched.
	 */
	public static CharSet toCharset( String aCharsetName ) {
		if ( aCharsetName == null || aCharsetName.isEmpty() ) {
			return null;
		}
		for ( CharSet eCharset : values() ) {
			if ( eCharset.name().toLowerCase().replace( "_", "" ).equals( aCharsetName.toLowerCase().replace( "_", "" ) ) ) {
				return eCharset;
			}
		}
		return null;
	}
}
