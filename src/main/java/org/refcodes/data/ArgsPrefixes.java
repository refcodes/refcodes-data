// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import java.util.HashSet;
import java.util.Set;

/**
 * This enumeration groups {@link ArgsPrefix} by platform.
 */
public enum ArgsPrefixes {

	/**
	 * Prefixes for command line arguments commonly used on Windows based
	 * systems.
	 */
	WINDOWS(ArgsPrefix.WINDOWS_SHORT_OPTION, ArgsPrefix.WINDOWS_LONG_OPTION),

	/**
	 * Prefixes for command line arguments commonly used on POSIX conforming
	 * systems.
	 */
	POSIX(ArgsPrefix.POSIX_SHORT_OPTION, ArgsPrefix.POSIX_LONG_OPTION);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ArgsPrefix[] _prefixes;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private ArgsPrefixes( ArgsPrefix... aPrefixes ) {
		_prefixes = aPrefixes;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the related {@link ArgsPrefix} elements.
	 * 
	 * @return The according {@link ArgsPrefix} elements.
	 */
	public ArgsPrefix[] getCommandArgPrefixes() {
		return _prefixes;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns an array with all {@link String} prefixes as of
	 * {@link ArgsPrefix#getPrefix()} defined by this enumeration.
	 * 
	 * @return The array with the according prefixes.
	 */
	public String[] toPrefixes() {
		final Set<String> thePrefixes = new HashSet<>();
		for ( ArgsPrefix ePrefix : _prefixes ) {
			thePrefixes.add( ePrefix.getPrefix() );
		}
		return thePrefixes.toArray( new String[thePrefixes.size()] );
	}
}
