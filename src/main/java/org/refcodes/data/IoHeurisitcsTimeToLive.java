// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied), as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.refcodes.mixin.TimeMillisAccessor;

/**
 * The {@link IoHeurisitcsTimeToLive} defines values used to cache IO related
 * heuristics (e.g. meta data of or for remote systems)
 */
public enum IoHeurisitcsTimeToLive implements TimeMillisAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CODE LOOP SLEEP TIMES:
	// /////////////////////////////////////////////////////////////////////////

	MIN(15000), // 15 sec.

	NORM(60000), // 1 min.

	MAX(600000); // 10 min.

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _value;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private IoHeurisitcsTimeToLive( int aValue ) {
		_value = aValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////
	@Override
	public int getTimeMillis() {
		return _value;
	}
}
