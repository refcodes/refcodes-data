// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

/**
 * A text region has a beginning index being included by the region and an end
 * index being excluded by the region and a resulting length.
 */
public class Region {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected int _beginIndex;
	protected int _endIndex;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link Region} instance.
	 * 
	 * @param aBeginIndex The beginning index (included) of the text region.
	 * @param aEndIndex The end index (being excluded) of the region.
	 */
	public Region( int aBeginIndex, int aEndIndex ) {
		_beginIndex = aBeginIndex;
		_endIndex = aEndIndex;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the beginning index of the region (being semantically included by
	 * the region being described).
	 * 
	 * @return The according beginning index (included).
	 */
	public int getBeginIndex() {
		return _beginIndex;
	}

	/**
	 * Returns the end index of the region (being semantically excluded by the
	 * region being described).
	 * 
	 * @return The according end index (excluded).
	 */
	public int getEndIndex() {
		return _endIndex;
	}

	/**
	 * Returns the length of the region as of {@link #getEndIndex()} -
	 * {@link #getBeginIndex()}.
	 * 
	 * @return The length of the region.
	 */
	public int getLength() {
		return _endIndex - _beginIndex;
	}
}
