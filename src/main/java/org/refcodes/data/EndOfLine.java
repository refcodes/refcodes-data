// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.refcodes.mixin.CharAccessor;

/**
 * Commonly used characters for EOL markers in texts.
 */
public enum EndOfLine implements CharAccessor {

	// @formatter:off
	CARRIAGE_RETURN ('\r'), 
	
	LINE_FEED('\n'); 
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private char _char;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private EndOfLine( char aChar ) {
		_char = aChar;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getChar() {
		return _char;
	}

	/**
	 * Tests if the given potential EOL character is contained in the
	 * {@link EndOfLine} enumeration.
	 *
	 * @param aEndOfLine the potential EOL character
	 * 
	 * @return True, if the given EOL character is contained in the
	 *         {@link EndOfLine} enumeration, else false.
	 */
	public static boolean isEndOfLine( char aEndOfLine ) {
		for ( EndOfLine eEndOfLine : values() ) {
			if ( eEndOfLine.getChar() == aEndOfLine ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns an array of the characters representing the EOL characters in
	 * this grouping enumeration.
	 * 
	 * @return The character array of the herein contained EOL characters.
	 */
	public static char[] toChars() {
		final char[] theChars = new char[values().length];
		for ( int i = 0; i < theChars.length; i++ ) {
			theChars[i] = values()[i].getChar();
		}
		return theChars;
	}
}
