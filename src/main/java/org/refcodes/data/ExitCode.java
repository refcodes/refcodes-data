// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.refcodes.mixin.StatusCodeAccessor;

/**
 * Some default exit codes defined for CLI applications.
 *
 * @author steiner
 */
public enum ExitCode implements StatusCodeAccessor<Integer> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	SUCCESS(0),

	CATCHALL(1),

	/**
	 * Missing keyword or command, or permission problem.
	 */
	MISUSE(2),

	/**
	 * Command invoked cannot execute, e.g. permission problem.
	 */
	CANNOT_EXECUTE(126),

	/**
	 * Possible problem with $PATH or a typo.
	 */
	NOT_FOUND(127),

	/**
	 * Script terminated by Control-C Ctl-C Control-C
	 */
	CONTROL_C(130);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _statusCode;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new exit code.
	 *
	 * @param aStatusCode the status code
	 */
	private ExitCode( int aStatusCode ) {
		_statusCode = aStatusCode;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	/**
	 * Gets the status code.
	 *
	 * @return the status code
	 */
	// /////////////////////////////////////////////////////////////////////////
	@Override
	public Integer getStatusCode() {
		return _statusCode;
	}
}
