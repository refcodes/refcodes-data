// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.refcodes.mixin.CharAccessor;
import org.refcodes.mixin.CodeAccessor;

/**
 * Some ASCII codes used by REFCODES.ORG artifacts.
 */
public enum Ascii implements CodeAccessor<Byte>, CharAccessor {

	/**
	 * ASCII(0) = &lt;NULL&gt;
	 * 
	 * "Null"
	 */
	NULL((byte) 0),

	/**
	 * ASCII(1) = &lt;SOH&gt;
	 * 
	 * "Start of Heading"
	 */
	SOH((byte) 1),

	/**
	 * ASCII(2) = &lt;STX&gt;
	 * 
	 * "Start of Text"
	 */
	STX((byte) 2),

	/**
	 * ASCII(3) = &lt;ETX&gt;
	 * 
	 * "End of Text"
	 */
	ETX((byte) 3),

	/**
	 * ASCII(4) = &lt;EOT&gt;
	 * 
	 * "End of Transmission"
	 */
	EOT((byte) 4),

	/**
	 * ASCII(5) = &lt;ENQ&gt;
	 * 
	 * "Enquiry"
	 */
	ENQ((byte) 5),

	/**
	 * ASCII(6) = &lt;ACK&gt;
	 * 
	 * "Acknowledgement"
	 */
	ACK((byte) 6),

	/**
	 * ASCII(7) = &lt;BELL&gt;
	 * 
	 * "BEL"
	 */
	BELL((byte) 7),

	/**
	 * * ASCII(16) = &lt;DLE&gt;
	 * 
	 * Data Link Escape
	 */
	DLE((byte) 16),

	/**
	 * ASCII(21) = &lt;NAK&gt;
	 * 
	 * "Negative Acknowledgement"
	 */
	NAK((byte) 21),

	/**
	 * ASCII(22) = &lt;SYN&gt;
	 * 
	 * "Synchronous Idle"
	 */
	SYN((byte) 22),

	/**
	 * ASCII(24) = &lt;CAN&gt;
	 * 
	 * "Cancel"
	 */
	CAN((byte) 24),

	/**
	 * ASCII(25) = &lt;EN&gt;
	 * 
	 * "End of Medium"
	 */
	EM((byte) 25),

	/**
	 * ASCII(27) = &lt;ESC&gt;
	 * 
	 * "Escape"
	 */
	ESC((byte) 27);

	private byte _asciiCode;

	private Ascii( byte aAsciiCode ) {
		_asciiCode = aAsciiCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Byte getCode() {
		return _asciiCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getChar() {
		return (char) ( _asciiCode & 0xFF );
	}
}
