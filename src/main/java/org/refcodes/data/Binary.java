// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.refcodes.mixin.ValueAccessor;

/**
 * Some values regarding binary numbers.
 */
public enum Binary implements ValueAccessor<Integer> {

	BYTES_PER_INT(4),

	BYTES_PER_LONG(8),

	BITS_PER_BYTE(8),

	BITS_PER_INT(BITS_PER_BYTE.getValue() * BYTES_PER_INT.getValue());

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _value;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new binary.
	 *
	 * @param aValue the value
	 */
	private Binary( int aValue ) {
		_value = aValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	// /////////////////////////////////////////////////////////////////////////
	@Override
	public Integer getValue() {
		return _value;
	}
}
