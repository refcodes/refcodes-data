// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

/**
 * The {@link Delimiters} groups {@link Delimiter} values belonging somehow
 * together or sharing a similar semantics.
 */
public enum Delimiters {

	PROPERTIES(new Delimiter[] { Delimiter.PATH, Delimiter.NAMESPACE }),

	PATHS(new Delimiter[] { Delimiter.PATH, Delimiter.DOS_PATH });

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Delimiter[] _delimiters;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new delimiters.
	 *
	 * @param aDelimiters the delimiters
	 */
	private Delimiters( Delimiter[] aDelimiters ) {
		_delimiters = aDelimiters;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Tests if the given delimiter is contained in the {@link Delimiters}
	 * definition.
	 *
	 * @param aDelimiter the delimiter
	 * 
	 * @return True, if the given delimiter is contained in the
	 *         {@link Delimiters} definition, else false.
	 */
	public boolean isDelimiter( char aDelimiter ) {
		for ( Delimiter eDelimiter : _delimiters ) {
			if ( eDelimiter.getChar() == aDelimiter ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Tests if the given delimiter is contained in the {@link Delimiters}
	 * definition.
	 *
	 * @param aDelimiter the delimiter
	 * 
	 * @return True, if the given delimiter is contained in the
	 *         {@link Delimiters} definition, else false.
	 */
	public boolean isDelimiter( Delimiter aDelimiter ) {
		for ( Delimiter eDelimiter : _delimiters ) {
			if ( eDelimiter == aDelimiter ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Contains an array of the characters representing the delimiters in this
	 * grouping enumeration.
	 * 
	 * @return The character array of the herein contained delimiters.
	 */
	public char[] getChars() {
		final char[] theChars = new char[_delimiters.length];
		for ( int i = 0; i < theChars.length; i++ ) {
			theChars[i] = _delimiters[i].getChar();
		}
		return theChars;
	}
}
