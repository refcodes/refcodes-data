// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

/**
 * The {@link Folders} define values useful when working with folders on a
 * file-system.
 */
public enum Folders {

	CONFIG_DIRS(new String[] { Folder.CONFIG.getName(), Folder.ETC.getName(), Folder.SETTINGS.getName(), Folder.DOT_CONFIG.getName(), Folder.DOT_SETTINGS.getName() });

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String[] _folderNames;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new folders.
	 *
	 * @param aFolderNames the folder names
	 */
	private Folders( String[] aFolderNames ) {
		_folderNames = aFolderNames;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * An array of the names representing the set of folder names.
	 * 
	 * @return The array of folder names representing this set.
	 */
	public String[] getNames() {
		return _folderNames;
	}
}
