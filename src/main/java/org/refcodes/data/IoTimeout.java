// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied), as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.refcodes.mixin.TimeMillisAccessor;

/**
 * The {@link IoTimeout} defines values used inside a IO processing control
 * flow.
 */
public enum IoTimeout implements TimeMillisAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CODE LOOP SLEEP TIMES:
	// /////////////////////////////////////////////////////////////////////////

	MIN(5000), // Influences the according AckTimeout!

	NORM(15000), // Influences the according AckTimeout!

	MAX(30000); // Influences the according AckTimeout!

	public static final int TIMEOUT_DIVIDER = 64;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _value;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new io timeout.
	 *
	 * @param aValue the value
	 */
	private IoTimeout( int aValue ) {
		_value = aValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////
	@Override
	public int getTimeMillis() {
		return _value;
	}

	/**
	 * Determines the sleep time within an I/O polling timeout loop.
	 * 
	 * @param aIoTimeoutInMs The overall I/O timeout.
	 * 
	 * @return The "recommended" loop sleep time during polling.
	 */
	public static long toTimeoutSleepLoopTimeInMs( long aIoTimeoutInMs ) {
		long thePollTimeInMs = aIoTimeoutInMs / TIMEOUT_DIVIDER;
		if ( thePollTimeInMs < 50 ) {
			thePollTimeInMs = 50;
		}
		return thePollTimeInMs;
	}

	/**
	 * Determines the timeout to securely sense for a given timeout.
	 * 
	 * @param aIoTimeoutInMs The overall I/O timeout within which to sense.
	 * 
	 * @return The "recommended" timeout to securely sense within the given
	 *         timeout.
	 */
	public static long toSenseTimeoutTimeframeInMs( long aIoTimeoutInMs ) {
		return aIoTimeoutInMs * 8 / 10;
	}
}
