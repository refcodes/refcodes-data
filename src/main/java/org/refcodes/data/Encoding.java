// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import org.refcodes.mixin.CodeAccessor;

/**
 * The {@link Encoding} define values representing an encoding which are to be
 * passed as argument to according methods.
 */
public enum Encoding implements CodeAccessor<String> {

	/**
	 * Encoding identifying UTF-8 when working with {@link String} objects.
	 */
	UTF_8("UTF-8"),

	/**
	 * This encoding is useful when having to ensure a 1-to-1 mapping between
	 * bytes and characters.
	 */
	ISO_8859_1("ISO-8859-1"),

	/**
	 * CP1252 (Code Page 1252) is a character encoding used in Microsoft Windows
	 * for Western European languages, such as English, French, German, Spanish,
	 * Portuguese, Italian, and others. CP1252 is not as widely used as it once
	 * was, as Unicode has become the dominant character encoding for modern
	 * software and the web. However, it is still important for maintaining
	 * compatibility with legacy systems and applications that use this
	 * encoding.
	 */
	CP1252("Cp1252"),

	/**
	 * MD5 hash encoding.
	 */
	MD5("MD5");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _code;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new encoding.
	 *
	 * @param aCode the code
	 */
	private Encoding( String aCode ) {
		_code = aCode;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCode() {
		return _code;
	}

	/**
	 * Determines weather the given string representation represents the
	 * according {@link Encoding}.
	 * 
	 * @param aEncoding The {@link Encoding}'s {@link String} representation
	 *        variant.
	 * 
	 * @return True in case the given encoding represents this {@link Encoding}.
	 */
	public boolean isEncoding( String aEncoding ) {
		if ( aEncoding != null && aEncoding.length() != 0 ) {
			return _code.toLowerCase().replace( " ", "" ).replace( "-", "" ).replace( "_", "" ).equals( aEncoding.toLowerCase().replace( " ", "" ).replace( "-", "" ).replace( "_", "" ) );
		}
		return false;
	}
}
