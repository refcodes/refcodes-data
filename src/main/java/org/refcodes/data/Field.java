// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

import java.util.Map;

import org.refcodes.mixin.NameAccessor;

/**
 * The {@link Field} are useful when working with fields defined for external
 * systems such as databases or when working with tables or {@link Map}
 * instances.
 */
public enum Field implements NameAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// Meta-Data FIELDS:
	// /////////////////////////////////////////////////////////////////////////

	META_CREATED_DATE("created_date"),

	META_MODIFIED_DATE("modified_date"),

	META_CREATED_BY("created_by"),

	META_MOFIFIED_BY("modified_by"),

	META_TIMESTAMP("timestamp"),

	META_VERSION("version"),

	LOG_LINE_NUMBER("log_line_number"),

	LOG_DATE("log_date"),

	LOG_PRIORITY("log_level"),

	LOG_THREAD_NAME("log_thread"),

	LOG_MESSAGE("log_message"),

	LOG_EXCEPTION("log_exception"),

	LOG_FULLY_QUALIFIED_CLASS_NAME("log_class_name"),

	LOG_METHOD_NAME("log_method_name"),

	LOG_CLASS_LINE_NUMBER("log_class_line_number"),

	LOG_SESSION_ID("log_session_id"),

	LOG_REQUEST_ID("log_request_id");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _name;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new field.
	 *
	 * @param aName the name
	 */
	private Field( String aName ) {
		_name = aName;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return _name;
	}
}