// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data;

/**
 * The {@link Numbers} interface defines some constants of interest related to
 * the various number types available.
 */
public interface Numbers {

	/**
	 * The minimum safe integer when floating point is involved, e.g. this
	 * minimum integer can be stored without loss in a double value. Evaluated
	 * to -2^53.
	 */
	long MIN_SAFE_INTEGER = -9007199254740992L;

	/**
	 * The maximum safe integer when floating point is involved, e.g. this
	 * maximum integer value can be stored without loss in a double value.
	 * Evaluates to 2^53-1.
	 */
	long MAX_SAFE_INTEGER = 9007199254740991L;

	/**
	 * The number of bytes required to store a value in the bounds
	 * [{@link #MIN_SAFE_INTEGER}..{@link #MAX_SAFE_INTEGER}].
	 */
	int SAFE_INTEGER_BYTES = 7;

}
